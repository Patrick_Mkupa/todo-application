﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoWebApplication.Middleware
{
    public static class Extensions
    {
        public static IApplicationBuilder UseETagHeaderMiddleware(this IApplicationBuilder builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            return builder.UseMiddleware<ETagMiddleware>();
        }

        //public static IApplicationBuilder UseProcessingTimeMiddleware(this IApplicationBuilder builder)
        //{
        //    if (builder == null)
        //    {
        //        throw new ArgumentNullException(nameof(builder));
        //    }

        //    return builder.UseMiddleware<ProcessTimeMiddleware>();
        //}

        //public static IApplicationBuilder UseRequestLoggerMiddleware(this IApplicationBuilder builder)
        //{
        //    if (builder == null)
        //    {
        //        throw new ArgumentNullException(nameof(builder));
        //    }

        //    return builder.UseMiddleware<RequestLoggerMiddleware>();
        //}


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoWebApplication.Models.AccountViewModels
{
    public class TodoViewModel
    {
        public Guid ID { get; set; }

        public String Title { get; set; }

        public String Category { get; set; }


        public bool isCompleted { get; set; }


        public DateTime DateCreated { get; set; }


        public DateTime DateCompleted { get; set; }


        public bool isReminderSet { get; set; }


        public DateTime DateReminder { get; set; }


        public String Description { get; set; }

        public String Priority { get; set; }

        public Guid UserID { get; set; }
    }
}

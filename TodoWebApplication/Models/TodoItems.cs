﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TodoWebApplication.Models
{
    public class TodoItems
    {

        public Guid ID { get; set; }
       
        public String Title { get; set; }
      
        public String Category { get; set; }

      
        public bool isCompleted { get; set; }

       
        public DateTime DateCreated { get; set; }

       
        public DateTime DateCompleted { get; set; }

       
        public bool isReminderSet { get; set; }


        public DateTime DateReminder { get; set; }

       
        public String Description { get; set; }

        public String Priority { get; set; }
        
        public Guid UserID { get; set; }

    }
}

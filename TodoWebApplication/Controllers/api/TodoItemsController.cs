﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TodoWebApplication.Data;
using TodoWebApplication.Models;
using Microsoft.AspNetCore.Authorization;
using TodoWebApplication.Models.AccountViewModels;

namespace TodoWebApplication.Controllers.api
{
    [Produces("application/json")]
    [Route("/api/Items")]
    [Authorize]
    public class TodoItemsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TodoItemsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/TodoItems
        [HttpGet("getall/{id}")]
        public async  Task<IActionResult> GetTodoItemsAsync([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            IEnumerable<TodoItems> Items =  _context.TodoItems.Where(m => m.UserID == id);

            if (Items == null)
            {
                return NoContent();
            }

            return Ok(Items);
        }

        // GET: api/TodoItems/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTodoItems([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var todoItems = await _context.TodoItems.SingleOrDefaultAsync(m => m.ID == id);

            if (todoItems == null)
            {
                return NotFound();
            }

            return Ok(todoItems);
        }

        // PUT: api/TodoItems/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTodoItems([FromRoute] Guid id, [FromBody] TodoItems todoItems)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != todoItems.ID)
            {
                return BadRequest();
            }

            _context.Entry(todoItems).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TodoItemsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TodoItems
        [HttpPost]
        public async Task<ActionResult> todo([FromBody] TodoItems todoItems)
        {
          //  return Ok(itemc);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.TodoItems.Add(todoItems);
           await _context.SaveChangesAsync();

            return Ok("Item Created Succesfully");
        }

        // DELETE: api/TodoItems/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTodoItems([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var todoItems = await _context.TodoItems.SingleOrDefaultAsync(m => m.ID == id);
            if (todoItems == null)
            {
                return NotFound();
            }

            _context.TodoItems.Remove(todoItems);
            await _context.SaveChangesAsync();

            return Ok(todoItems);
        }

        private bool TodoItemsExists(Guid id)
        {
            return _context.TodoItems.Any(e => e.ID == id);
        }
    }
}
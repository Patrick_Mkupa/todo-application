﻿namespace TodoWebApplication.Controllers.api
{
    public class IdentityCookieOptions
    {
        public string ExternalCookieAuthenticationScheme { get; internal set; }
    }
}
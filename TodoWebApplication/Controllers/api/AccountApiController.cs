﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using TodoWebApplication.Data;
using TodoWebApplication.Services;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TodoWebApplication.Models.AccountViewModels;
using Microsoft.AspNetCore.Authorization;
using TodoWebApplication.Middleware;
using TodoWebApplication.Models;

namespace TodoWebApplication.Controllers.api
{
    [Produces("application/json")]
    [Route("/[action]")]
    public class AccountApiController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
     
        private readonly ILogger _logger;
        private readonly string _externalCookieScheme;
        public AccountApiController(
          UserManager<ApplicationUser> userManager,
          SignInManager<ApplicationUser> signInManager,
          IOptions<IdentityCookieOptions> identityCookieOptions,
          IEmailSender emailSender,
         
          ILoggerFactory loggerFactory)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _externalCookieScheme = identityCookieOptions.Value.ExternalCookieAuthenticationScheme;
            _emailSender = emailSender;
     
            _logger = loggerFactory.CreateLogger<AccountApiController>();
        }

        // POST: /Account/Register
        [HttpPost]

        public ActionResult Register([FromBody] RegisterViewModel content)
        {
            if(!ModelState.IsValid){
                return BadRequest(ModelState);
            }
            var user = new ApplicationUser { UserName = content.Email, Email = content.Email };
            var result =  _userManager.CreateAsync(user, content.Password);

                _logger.LogInformation(3, "User created a new account with password.");


                return Ok(result);
            
          
           
        }


        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [Produces("application/json")]
        public async Task<IActionResult> Logout()
        {

            if (ModelState.IsValid)
            {

               await _signInManager.SignOutAsync();
             

                    return Ok("Logout Succesfulll");
                
              
            }

            // If we got this far, something failed, redisplay form
            return BadRequest();
        }


    }
}